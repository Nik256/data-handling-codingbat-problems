package demo;

import service.ProblemService;

public class DemoService {
    private ProblemService problemService;

    public DemoService(ProblemService problemService) {
        this.problemService = problemService;
    }

    public void execute() {
        System.out.println("String-3 > maxBlock");
        System.out.println("maxBlock(\"hoopla\") → " + problemService.maxBlock("hoopla"));
        System.out.println("maxBlock(\"abbCCCddBBBxx\") → " + problemService.maxBlock("abbCCCddBBBxx"));
        System.out.println("maxBlock(\"\") → " + problemService.maxBlock(""));
        System.out.println("------------------------------------------------------------------");

        System.out.println("String-3 > notReplace");
        System.out.println("notReplace(\"is test\") → " + problemService.notReplace("is test"));
        System.out.println("notReplace(\"is-is\") → " + problemService.notReplace("is-is"));
        System.out.println("notReplace(\"This is right\") → " + problemService.notReplace("This is right"));
        System.out.println("------------------------------------------------------------------");

        System.out.println("String-3 > sameEnds");
        System.out.println("sameEnds(\"abXYab\") → " + problemService.sameEnds("abXYab"));
        System.out.println("sameEnds(\"xx\") → " + problemService.sameEnds("xx"));
        System.out.println("sameEnds(\"xxx\") → " + problemService.sameEnds("xxx"));
        System.out.println("------------------------------------------------------------------");

    }
}
