import demo.DemoService;
import service.ProblemService;

public class Main {

    public static void main(String[] args) {
        new DemoService(new ProblemService()).execute();
    }
}
