package service;

public class ProblemService {
    /*
    String-3 > maxBlock
    Given a string, return the length of the largest "block" in the string. A block is a run of adjacent chars that are the same.

    maxBlock("hoopla") → 2
    maxBlock("abbCCCddBBBxx") → 3
    maxBlock("") → 0
    */
    public int maxBlock(String str) {
        int blockLength = 1;
        int maxBlockLength = 0;
        if (str.length() == 0)
            return 0;
        for (int i = 0; i < str.length() - 1; i++) {
            if (str.charAt(i) == str.charAt(i + 1))
                blockLength++;
            else
                blockLength = 1;

            if (blockLength > maxBlockLength)
                maxBlockLength = blockLength;
        }
        return maxBlockLength;
    }

    /*
    String-3 > notReplace
    Given a string, return a string where every appearance of the lowercase word "is" has been replaced with "is not".
    The word "is" should not be immediately preceeded or followed by a letter -- so for example the "is" in "this" does
    not count. (Note: Character.isLetter(char) tests if a char is a letter.)

    notReplace("is test") → "is not test"
    notReplace("is-is") → "is not-is not"
    notReplace("This is right") → "This is not right"
    */
    public String notReplace(String str) {
        StringBuilder result = new StringBuilder();
        result.append(" ");
        result.append(str);
        result.append(" ");

        for (int i = 1; i < result.length() - 2; i++) {
            if ((result.substring(i, i + 2).equals("is")
                    && !Character.isLetter(result.charAt(i - 1))
                    && !Character.isLetter(result.charAt(i + 2)))) {
                result.replace(i, i + 2, "is not");
            }
        }
        return result.toString().trim();
    }

    /*
    String-3 > sameEnds
    Given a string, return the longest substring that appears at both the beginning and end of the string without
    overlapping. For example, sameEnds("abXab") is "ab".

    sameEnds("abXYab") → "ab"
    sameEnds("xx") → "x"
    sameEnds("xxx") → "x"
    */
    public String sameEnds(String string) {
        int length = string.length();
        String result = "";
        for (int i = 0; i < length / 2; i++) {
            if (string.substring(0, i + 1).equals(string.substring(length - i - 1, length)))
                result = string.substring(0, i + 1);
        }
        return result;
    }
}
